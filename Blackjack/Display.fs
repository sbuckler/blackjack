﻿module Blackjack.Display

open System

let private (==@) string1 string2 =
    String.Equals(string1, string2, StringComparison.CurrentCultureIgnoreCase)

let private displayScore pronoun playerScore =
    printfn "\n%s score is %i" pronoun playerScore

let private displayHand pronoun playerHand =
    printfn "\n%s hand:" pronoun
    playerHand |> List.iter (fun card -> printfn "%A" card)

let rec getPlayerAction message =
    if String.IsNullOrWhiteSpace message then printfn "" else printfn "\n%s" message
    printfn "Type 'stand' or 'hit'..."

    match Console.ReadLine() with
    | s when s ==@ "hit" -> Hit
    | s when s ==@ "stand" -> Stand
    | _ -> getPlayerAction "That is not an appropriate command."

let rec dealPlayerIn message =
    if String.IsNullOrWhiteSpace message then printfn "" else printfn "\n%s" message
    printfn "Type 'deal' to begin, 'quit' to exit..."

    match Console.ReadLine() with
    | s when s ==@ "deal" -> Deal
    | s when s ==@ "quit" -> Quit
    | _ -> dealPlayerIn "That is not an appropriate command."

let displayDealerCard dealerCard =
    printfn "\nThe dealer is showing %A " dealerCard

let displayStand message =
    if String.IsNullOrWhiteSpace message then printfn "" else printfn "\n%s" message

    Stand

let quitGame message =
    if String.IsNullOrWhiteSpace message then printfn "" else printfn "\n%s" message

    Quit

let rec restartGame yesCommand noCommand message =
    if String.IsNullOrWhiteSpace message then printfn "" else printfn "\n%s" message
    printfn "Type '%s' to play again, or '%s' to quit..." yesCommand noCommand
    
    match Console.ReadLine() with
    | s when s ==@ yesCommand -> Deal
    | s when s ==@ noCommand -> Quit
    | _ -> restartGame yesCommand noCommand "That is not an appropriate command."

let displayDealerScoreAndHand message dealerScore dealerHand dealerGameState =
    displayScore "Dealer" dealerScore
    displayHand "Dealer" dealerHand
    printfn ""

    match dealerGameState with
    | Blackjack -> printfn "Dealer Blackjack!"
    | Bust -> printfn "The dealer busts!"
    | _ -> printfn ""

    quitGame message

let displayScoreAndHand playerScore playerHand playerGameState =
    displayScore "Your" playerScore
    displayHand "Your" playerHand
    printfn ""

    match playerGameState with
    | Blackjack -> printfn "Blackjack!"
    | Bust -> printfn "Sorry, you bust."
    | Open -> printf ""

let setupDisplay () =
    Console.Title <- "F# Blackjack by Stephen Buckler"
    Console.BackgroundColor <- ConsoleColor.DarkGray
    Console.Clear()
    Console.ForegroundColor <- ConsoleColor.White
    Console.SetWindowSize(Console.LargestWindowWidth / 2, Console.LargestWindowHeight / 2)