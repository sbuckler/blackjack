﻿open Blackjack.Game
open Blackjack.Display

[<EntryPoint>]
let main args =
    setupDisplay () |> ignore
    startGame () |> ignore

    0