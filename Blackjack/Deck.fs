﻿module Blackjack.Deck

open System

let private createDecks numberOfDecks =
    let allSuits = [ Diamonds; Hearts; Clubs; Spades ]

    let allRanks = [
        Two; Three; Four; Five; Six; Seven; Eight; Nine; Ten;
        Jack; Queen; King; Ace ]
        
    [ for i = 1 to numberOfDecks do
        yield [ for suit in allSuits do
                    for rank in allRanks do
                        yield (suit, rank) ] ]

let private shuffleDecks decks =
    let random = new Random()
    
    List.concat decks
    |> List.sortBy (fun card -> random.Next())

let createAndShuffleDecks = createDecks >> shuffleDecks

let deal deck =
    match deck with
    | card::remainingDeck -> (Some card, remainingDeck)
    | [] -> (None, [])