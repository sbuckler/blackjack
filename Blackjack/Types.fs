﻿[<AutoOpen>]
module Blackjack.Types

type Suit =
    | Spades
    | Hearts
    | Clubs
    | Diamonds

type Rank =
    | Two
    | Three
    | Four 
    | Five 
    | Six
    | Seven
    | Eight
    | Nine
    | Ten
    | Jack
    | Queen
    | King
    | Ace

type GameState =
    | Blackjack
    | Bust
    | Open

type GameAction =
    | Deal
    | Hit
    | Stand
    | Quit

type VictoryState =
    | House
    | Player
    | Push