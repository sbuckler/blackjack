﻿module Blackjack.Game

open Blackjack.Deck
open Blackjack.Score
open Blackjack.Display

let private getGameState handScore =
    match handScore with
    | 21 -> Blackjack
    | s when s > 21 -> Bust
    | _ -> Open

let private advancePlayerGameState playerCard remainingDeck playerHand =
    let currentPlayerHand = playerCard::playerHand
    let playerScore = score currentPlayerHand
    let playerGameState = getGameState playerScore

    displayScoreAndHand playerScore currentPlayerHand playerGameState

    match playerGameState with
    | Open -> (Deal, remainingDeck, currentPlayerHand, playerScore)
    | Blackjack -> (Stand, remainingDeck, currentPlayerHand, playerScore)
    | Bust -> (Quit, remainingDeck, currentPlayerHand, playerScore)

let private continuePlayerActions deck playerHand =
    let (playerCard, remainingDeck) = deal deck

    match playerCard with
    | Some pCard -> advancePlayerGameState pCard remainingDeck playerHand
    | None -> (Quit, [], [], 0)

let rec private advanceDealerGameState dealerHand deck playerScore =
    let (dealerCard, remainingDeck) = deal deck

    let currentDealerHand = match dealerCard with
    | Some dCard -> dCard::dealerHand
    | None -> dealerHand
    
    let dealerScore = score currentDealerHand
    let dealerGameState = getGameState dealerScore

    if currentDealerHand.Length > dealerHand.Length then
        match dealerScore with
        | dScore when dScore > playerScore && dealerGameState <> Bust -> (House, dScore, currentDealerHand, dealerGameState)
        | dScore when dScore = playerScore && dealerGameState = Blackjack -> (Push, dScore, currentDealerHand, dealerGameState)
        | dScore when dScore < playerScore || dScore = playerScore -> advanceDealerGameState currentDealerHand remainingDeck playerScore
        | dScore -> (Player, dScore, currentDealerHand, dealerGameState)
    else
        (Push, dealerScore, currentDealerHand, dealerGameState)

let private continueDealerActions message dealerShownCard deck playerScore =
    displayStand message |> ignore

    advanceDealerGameState [dealerShownCard] deck playerScore

let private beginDealerActions message dealerShownCard deck playerScore =
    match continueDealerActions message dealerShownCard deck playerScore with
    | (Player, dScore, dHand, dGameState) -> displayDealerScoreAndHand "The game is over. You win!" dScore dHand dGameState
    | (House, dScore, dHand, dGameState) -> displayDealerScoreAndHand "The game is over. The house wins!" dScore dHand dGameState
    | (Push, dScore, dHand, dGameState) -> displayDealerScoreAndHand "The game is over. The game was a push." dScore dHand dGameState

let private showDealerCard dealerCard remainingDeck =
    displayDealerCard dealerCard

    (dealerCard, remainingDeck)

let private dealDealerIn deck =
    let (dealerCard, remainingDeck) = deal deck

    match dealerCard with
    | Some card ->  showDealerCard card remainingDeck
    | None -> (deck.Head, [])

let rec private playGame dealerShownCard deck hand playerScore =
    match getPlayerAction "" with
    | Hit ->
        match continuePlayerActions deck hand with
        | (Quit, [], [], pScore) -> quitGame "Out of cards..."
        | (Deal, restOfDeck, pHand, pScore) -> playGame dealerShownCard restOfDeck pHand pScore
        | (Stand, restOfDeck, pHand, pScore) -> beginDealerActions "It is now the dealer's turn." dealerShownCard restOfDeck pScore
        | _ -> quitGame "The game is over. The house wins!"
    | Stand -> beginDealerActions "You have chosen to stand." dealerShownCard deck playerScore
    | _ -> Quit

let beginGame deck =
    let (dealerShownCard, remainingDeck) = dealDealerIn deck

    playGame dealerShownCard remainingDeck [] 0

let rec startGame () =
    match dealPlayerIn "Welcome to the table!" with
    | Deal ->
        match beginGame (createAndShuffleDecks 6) with
        | Quit ->
            match restartGame "y" "n" "Would you like to play again?" with
            | Deal -> startGame ()
            | _ -> Quit
        | _ -> Deal
    | Quit -> quitGame "Thanks for playing!"
    | _ -> quitGame "Unexpected game state occurred."