﻿module Blackjack.Score

let private getCardValue card =
    match card with
    | (_, Two) -> 2
    | (_, Three) -> 3
    | (_, Four) -> 4
    | (_, Five) -> 5
    | (_, Six) -> 6
    | (_, Seven) -> 7
    | (_, Eight) -> 8
    | (_, Nine) -> 9
    | (_, Ten) -> 10
    | (_, Jack) -> 10
    | (_, Queen) -> 10
    | (_, King) -> 10
    | (_, Ace) -> 11

let private cardIsAce card =
    match card with
    | (_, Ace) -> true
    | _ -> false

let private adjustScoreForAces currentScore =
    match currentScore with
    | s when s > 21 -> currentScore - 10
    | _ -> currentScore

let score hand =
    let currentScore =
        hand
        |> List.map getCardValue
        |> List.sum

    hand
    |> List.filter cardIsAce
    |> List.fold (fun adjustedScore ace -> adjustScoreForAces adjustedScore) currentScore